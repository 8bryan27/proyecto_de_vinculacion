//Pines Sensor ultrasonico
const int Echo = 5;
const int Trig = 6;

//TIEMPOS
//Contador de tiempo
int tAct = 0;
int tPas = 0;
int tiempoTranscurrido = 0;


// VARIABLES
long duracion;                          //Tiempo que dura en reflejarse la onda emitida por el sensor ultrasonico
long distancia;                         //Distancia entre el objeto y el ultrasonico


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  pinMode(Echo, INPUT);
  pinMode(Trig, OUTPUT);
  
}

void loop() {
  // put your main code here, to run repeatedly:

  tAct = millis();
  tiempoTranscurrido = tAct - tPas;

  if (tiempoTranscurrido >= 1000) {
    tPas = tAct;
    ultrasonico();
  }
  
}


void ultrasonico() {
  digitalWrite(Trig, HIGH);               // genera el pulso de triger por 10ms
  delayMicroseconds(10);
  digitalWrite(Trig, LOW);
  duracion = pulseIn(Echo, HIGH);         //Cuenta el tiempo que demora en regresar la onda
  distancia = duracion * 0.034 / 2;        // calcula la distancia en centimetros
  Serial.println("LA DISTANCIA MEDIDA ES:");
  Serial.println(distancia);
  Serial.println("cm");
}
