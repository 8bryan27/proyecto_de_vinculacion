//Pines Sensor ultrasonico
const int Echo = 5;
const int Trig = 6;
const int estado1LED = 10;
const int estado0LED = 7;
const int pulsador = 12;

//TIEMPOS
//Contador de tiempo
int tAct = 0;
int tPas = 0;
int tiempoTranscurrido = 0;


// VARIABLES
long duracion;                          //Tiempo que dura en reflejarse la onda emitida por el sensor ultrasonico
long distancia;                         //Distancia entre el objeto y el ultrasonico

int estado;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  pinMode(Echo, INPUT);
  pinMode(Trig, OUTPUT);
  pinMode(pulsador, OUTPUT);
  pinMode(estado1LED, OUTPUT);
  pinMode(estado0LED, OUTPUT);
    
  /*
  digitalWrite(pulsador, LOW);
  delay(100);
  digitalWrite(pulsador, HIGH);
  delay(1000);
  */
}

void loop() {
  // put your main code here, to run repeatedly:

  tAct = millis();
  tiempoTranscurrido = tAct - tPas;


  maquinadeestados();
  Serial.print("ESTADO ");
  Serial.println(estado);
}


void maquinadeestados() {
  switch (estado) {
    case (0):
      estado0();

      if (distancia <= 20 && distancia != 0)
      {
        distancia = 0;
        estado = 1;
        tPas = tAct;
      }
      /*
        if( tiempoTranscurrido <= 2000)
        {
        estado = 0;
        } else
              {
                tPas = tAct;
                if (distancia <= 20 && distancia != 0)
                                                     {
                                                      distancia = 0;
                                                      estado = 1;
                                                     }
              }
      */
      break;

    case (1):

      estado1();
      if ( tiempoTranscurrido < 5000) {
        estado = 1;

      } else
      {
        estado = 0;
      }

      break;

    default:
      estado = 0;
      break;
  }
}


void estado0() {
  digitalWrite(estado0LED, HIGH);
digitalWrite(estado1LED, LOW);
digitalWrite(pulsador, HIGH); 
  if (tiempoTranscurrido >= 2000) {
    tPas = tAct;
    ultrasonico();
  }
}

void estado1() {
digitalWrite(estado0LED, LOW);
digitalWrite(estado1LED, HIGH);
if ( tiempoTranscurrido < 200){
      digitalWrite(pulsador, LOW);     
    } else 
          {
            digitalWrite(pulsador, HIGH);
          }
}
/*
  digitalWrite(pulsador, LOW);
  delay(1000);

  digitalWrite(pulsador, HIGH);
*/



void ultrasonico() {
  digitalWrite(Trig, HIGH);               // genera el pulso de triger por 10ms
  delayMicroseconds(10);
  digitalWrite(Trig, LOW);
  duracion = pulseIn(Echo, HIGH);         //Cuenta el tiempo que demora en regresar la onda
  distancia = duracion * 0.034 / 2;        // calcula la distancia en centimetros
  Serial.println("LA DISTANCIA MEDIDA ES:");
  Serial.println(distancia);
  Serial.println("cm");
}
